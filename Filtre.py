from sqlalchemy.orm import Session

from Bijou import Bijou
from Db import engine
from Meta import get_selection, get_selection_name


class Filtres:

    session = Session(bind=engine)

    def __init__(self):
        pass

    def get_selection(self, tag, size):
        # Tag spéciaux
        if tag == 'accueil':
            return [self.get_selection('selection1', size), self.get_selection('selection2', size)]

        ids = get_selection(tag)
        res = {'nom': get_selection_name(tag), 'bijoux': []}

        for bijou_id in ids:
            bijou = self.session.query(Bijou).filter_by(id=bijou_id, treated=True, banned=False).first()
            if bijou is None:
                return {'error': 'bijou not found'}

            if size == 'short':
                if len(res['bijoux']) < 5:
                    res['bijoux'].append(bijou.simple_form())
            else:
                res['bijoux'].append(bijou.simple_form())

        return res

    def get_user_selection(self, user):
        pass
