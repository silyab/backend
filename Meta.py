SELECTIONS = {
    'selection1': [12, 1773, 1774, 12, 1773, 12],
    'selection2': [1773, 12, 1774, 12, 1773, 12, 12],
}

SELECTIONS_NAMES = {
    'selection1': 'Selection 1',
    'selection2': 'Selection 2',
}


def get_selection(tag):
    return SELECTIONS[tag]


def get_selection_name(tag):
    return SELECTIONS_NAMES[tag]

MARQUES = {
    'marque': {},
}


def info_source(marque):
    return MARQUES[marque]
