from __future__ import division, print_function, absolute_import

import machine_learning.features as feat
import tflearn
from PIL import Image
from sqlalchemy.orm import Session
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation

from Bijou import Bijou
from Db import engine

session = Session(bind=engine)
categorie = 'Bague'
field = feat.get_field()
size = 32, 32
nbr_to_predict = 200

bijoux = session.query(Bijou).filter_by(banned=False, categorie=categorie)
count = bijoux.count()

i = 0
X = []
ids = []
# Preparation X
print('Generating X...')
for bijou in bijoux:
    if not feat.bijou_is_ok(bijou, field):

        # Lecture et conversion le l'image en 32*32*3 (rgb)
        im = Image.open("./pics/bijou_" + str(bijou.id) + ".jpg")

        X.append(feat.extract_X(im, size, field))

        ids.append(bijou.id)

        i += 1
        if i % 50 == 0:
            print(str(i)+'/'+str(nbr_to_predict))
        if i >= nbr_to_predict:
            break

print('done, X length :' + str(len(X)))

# Same network definition as before
img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()
img_aug = ImageAugmentation()
img_aug.add_random_flip_leftright()
img_aug.add_random_rotation(max_angle=25.)
img_aug.add_random_blur(sigma_max=3.)

network = input_data(shape=[None, 32, 32, 3],
                     data_preprocessing=img_prep,
                     data_augmentation=img_aug)
network = conv_2d(network, 32, 3, activation='relu')
network = max_pool_2d(network, 2)
network = conv_2d(network, 64, 3, activation='relu')
network = conv_2d(network, 64, 3, activation='relu')
network = max_pool_2d(network, 2)
network = fully_connected(network, 512, activation='relu')
network = dropout(network, 0.5)
network = fully_connected(network, feat.get_len_conversion(field), activation='softmax')
network = regression(network, optimizer='adam',
                     loss='categorical_crossentropy',
                     learning_rate=0.001)

model = tflearn.DNN(network, tensorboard_verbose=0, checkpoint_path="./classifier/"+field+'-classifier.tfl.ckpt')
model.load("./classifier/"+field+"-classifier.tfl")

# Predict
prediction = model.predict(X)

# Check the result
for i in range(len(prediction)):
    bijou = session.query(Bijou).filter_by(id=ids[i]).first()
    feat.predict(bijou, prediction[i], field)
    session.add(bijou)

session.commit()
