import os
from urllib import request

from sqlalchemy.orm import Session

from Bijou import Bijou
from Db import engine

session = Session(bind=engine)
categorie = 'Bague'

bijoux = session.query(Bijou).filter_by(banned=False, categorie=categorie)
count = bijoux.count()
i = 0
for bijou in bijoux:
    file_path = "./pics/bijou_"+str(bijou.id)+".jpg"
    if not os.path.exists(file_path):
        url = bijou.images['thumb']['url']
        request.urlretrieve(url, file_path)

    i += 1
    if i % 50 == 0:
        print(str(i)+'/'+str(count))
