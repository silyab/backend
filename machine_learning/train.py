from __future__ import division, print_function, absolute_import

import tflearn
from PIL import Image
from sqlalchemy.orm import Session
from tflearn.data_utils import shuffle
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation

from Bijou import Bijou
from Db import engine
import machine_learning.features as feat

session = Session(bind=engine)
categorie = 'Bague'
field = feat.get_field()
size = 32, 32

bijoux = session.query(Bijou).filter_by(banned=False, categorie=categorie)
count = bijoux.count()

i = 0
X = []
Y = []
# Preparation X et Y
print('Generating X and Y...')
for bijou in bijoux:
    if feat.bijou_is_ok(bijou, field):

        # Lecture et conversion le l'image en 32*32*3 (rgb)
        im = Image.open("./pics/bijou_" + str(bijou.id) + ".jpg")

        X.append(feat.extract_X(im, size, field))

        # preparation de Y
        Y.append(feat.extract_Y(bijou, field))

        i += 1
        if i % 50 == 0:
            print(str(i)+'/'+str(count))

print('done, X length :' + str(len(X)))

# Shuffle the data
X, Y = shuffle(X, Y)

# Extracting 10% of test data
l = int(len(X)*0.9)
X_test = X[l:]
X = X[:l]
Y_test = Y[l:]
Y = Y[:l]

# Make sure the data is normalized
img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()

# Create extra synthetic training data by flipping, rotating and blurring the
# images on our data set.
img_aug = ImageAugmentation()
img_aug.add_random_flip_leftright()
img_aug.add_random_rotation(max_angle=25.)
img_aug.add_random_blur(sigma_max=3.)

# Define our network architecture:

# Input is a 32x32 image with 3 color channels (red, green and blue)
network = input_data(shape=[None, 32, 32, 3],
                     data_preprocessing=img_prep,
                     data_augmentation=img_aug)

# Step 1: Convolution
network = conv_2d(network, 32, 3, activation='relu')

# Step 2: Max pooling
network = max_pool_2d(network, 2)

# Step 3: Convolution again
network = conv_2d(network, 64, 3, activation='relu')

# Step 4: Convolution yet again
network = conv_2d(network, 64, 3, activation='relu')

# Step 5: Max pooling again
network = max_pool_2d(network, 2)

# Step 6: Fully-connected 512 node neural network
network = fully_connected(network, 512, activation='relu')

# Step 7: Dropout - throw away some data randomly during training to prevent over-fitting
network = dropout(network, 0.5)

# Step 8: Fully-connected neural network with len(conversion) outputs to make the final prediction
network = fully_connected(network, feat.get_len_conversion(field), activation='softmax')

# Tell tflearn how we want to train the network
network = regression(network, optimizer='adam',
                     loss='categorical_crossentropy',
                     learning_rate=0.001)

# Wrap the network in a model object
model = tflearn.DNN(network, tensorboard_verbose=0, checkpoint_path="./classifier/"+field+'-classifier.tfl.ckpt')

# Train it! We'll do 100 training passes and monitor it as it goes.
model.fit(X, Y, n_epoch=100, shuffle=True, validation_set=(X_test, Y_test),
          show_metric=True, batch_size=96,
          run_id=field+'-classifier')

# Save model when training is complete to a file
model.save("./classifier/"+field+"-classifier.tfl")
print("Network trained and saved as "+field+"-classifier.tfl!")
