import numpy as np
from PIL import Image, ImageOps


def get_field():
    return 'taille'


def extract_X(im, size, field):

    im.thumbnail(size, Image.ANTIALIAS)
    thumb = ImageOps.fit(im, size, Image.ANTIALIAS)
    rgb = thumb.convert('RGB')

    res = []
    for j in range(size[0]):
        res.append([])
        for k in range(size[1]):
            res[j].append([])
            r, g, b, = rgb.getpixel((j, k))
            res[j][k] = [float(r), float(g), float(b)]

    return res


conversion = {
    'couleur_metal': ['argente', 'dore', 'rose', 'noir', 'autre'],
    'couleurs': ['argente', 'dore', 'rose', 'noir', 'autre'],
    'taille': ['tres_discret', 'discret', 'normal', 'imposant', 'tres_imposant'],
}


def extract_Y(bijou, field):
    res = None
    if field in ['couleur_metal', 'taille']:
        res = [0] * len(conversion[field])
        res[conversion[field].index(bijou.data[field])] = 1

    return res


def bijou_is_ok(bijou, field):
    if field in ['couleur_metal', 'taille']:
        return field in bijou.data
    if field == 'couleurs':
        return bijou.treated and not bijou.banned


def get_len_conversion(field):
    if field in ['couleur_metal', 'couleurs', 'taille']:
        return len(conversion[field])


# noinspection PyTypeChecker
def predict(bijou, prediction, field):
    if field in ['couleur_metal', 'taille']:
        bijou.data['p_' + field] = conversion[field][np.argmax(prediction)]
