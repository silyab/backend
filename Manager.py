import random
import string
import traceback

import time
from sqlalchemy.orm import Session

from Bijou import Bijou
from Db import engine
from Filtre import Filtres
from User import User
from machine_learning.features import get_field


class Manager:

    session = Session(bind=engine)
    filtres = Filtres()

    def __init__(self):
        pass

    # Users

    # Renvoi un nouvel id valable
    def get_new_id(self):
        try:
            while True:
                user_id = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits)
                                  for _ in range(20))

                if self.session.query(User).filter_by(user_id=user_id).first() is None:
                    break
        except:
            print_error('get_new_id')
            return {'error': 'error'}

        return user_id

    # Test l'existance d'un id
    def test_id(self, user_id):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}
            return 'ok'
        except:
            print_error(user_id)
            return {'error': 'error'}

    # Cree un nouvel utilisateur
    def create_user(self, user_id):
        try:
            new_user = User(user_id)
            self.session.add(new_user)
            self.session.commit()
            return 'ok'
        except:
            self.session.rollback()
            print_error(user_id)
            return {'error': 'error'}

    # Modifie un utilisateur
    def update_user(self, user_id, data):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}

            user.update_info(data)
            self.session.add(user)
            self.session.commit()
            return 'ok'
        except:
            self.session.rollback()
            print_error(user_id)
            return {'error': 'error'}

    # Retourne info d'un utilisateur
    def info_user(self, user_id):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}

            return user.get_info()
        except:
            print_error(user_id)
            return {'error': 'error'}

    # renvoi un user selon son user_id
    def _get_user(self, user_id):

        user = self.session.query(User).filter_by(user_id=user_id).first()

        return user

    # Wishlist

    # Retourne wishlist d'un utilisateur
    def get_wishlist(self, user_id, size):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}

            return user.get_wishlist(size)
        except:
            print_error(user_id)
            return {'error': 'error'}

    # Modifie wishlist d'un utilisateur
    def update_wishlist(self, user_id, data):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}

            if 'add' not in data or 'bijou_id' not in data:
                return {'error': 'invalid data'}

            bijou = self._get_bijou(data['bijou_id'])
            if 'error' in bijou:
                return bijou

            return user.update_wishlist(bijou, data['add'])
        except:
            print_error(user_id)
            return {'error': 'error'}

    # Envoi par email wishlist d'un utilisateur
    def email_wishlist(self, user_id, data):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}

            if 'email' not in data:
                return {'error': 'invalid data'}

            return user.email_wishlist(data['email'])
        except:
            print_error(user_id)
            return {'error': 'error'}

    # Selections

    # Retourne selection selon le tag
    def get_selection(self, tag, size):
        try:
            return self.filtres.get_selection(tag, size)
        except:
            print_error('selection : '+tag)
            return {'error': 'error'}

    # Retourne selection de l'utilisateur selon ses filtres
    def get_user_selection(self, user_id):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}

            return self.filtres.get_user_selection(user)
        except:
            print_error(user_id)
            return {'error': 'error'}

    # Bijoux

    # Retourne info sur un bijou selon le user
    def get_info_bijou(self, user_id, bijou_id):
        try:
            user = self._get_user(user_id)
            if user is None:
                return {'error': 'not found'}

            bijou = self._get_bijou(bijou_id)
            if 'error' in bijou:
                return bijou

            return bijou.get_info(user)
        except:
            print_error(user_id+' bijou_id : '+bijou_id)
            return {'error': 'error'}

    # renvoi un bijou selon son id
    def _get_bijou(self, bijou_id):

        bijou = self.session.query(Bijou).filter_by(id=bijou_id, treated=True, banned=False).first()
        if bijou is None:
            return {'error': 'bijou not found'}

        return bijou

    # Train

    # retourne info disponible sur un bijou, si id=0 retourne un bijou non traité
    def get_train(self, bijou_id):
        try:
            categorie = 'Bague'
            prix_max = 501

            if bijou_id != '0':
                bijou_not_treated = self.session.query(Bijou).filter_by(id=bijou_id)
                bijou = bijou_not_treated.first()
                left = bijou_not_treated.count()
                done = 0
            else:
                bijou_not_treated = self.session.query(Bijou)\
                    .filter_by(treated=False, banned=False, categorie=categorie)
                bijou_treated = self.session.query(Bijou)\
                    .filter_by(treated=True, banned=False, categorie=categorie)

                left = bijou_not_treated.count()
                done = bijou_treated.count()

                i = 0
                bijou = bijou_not_treated[i]
                while i < bijou_not_treated.count() and (float(bijou.prix) > prix_max or 'taille' not in bijou.data):
                    i += 1
                    bijou = bijou_not_treated[i]
                    left -= 1

            if bijou is None or (float(bijou.prix) > prix_max and bijou_id == '0'):
                return {'left': left, 'done': done}
            else:
                res = bijou.train_form()
                res['left'] = left
                res['done'] = done
                return res
        except:
            print_error('get train bijou_id : '+bijou_id)
            return {'error': 'error'}

    # sauvegarde un bijou
    def post_train(self, bijou_id, data):
        try:
            bijou = self.session.query(Bijou).filter_by(id=bijou_id).first()
            if bijou is None:
                return {'error': 'bijou not found'}

            bijou.set_data(data)
            self.session.add(bijou)
            self.session.commit()
            return 'ok'
        except:
            self.session.rollback()
            print_error('train bijou_id : '+bijou_id)
            return {'error': 'error'}

    # retourne liste de bijoux pre-traités
    def get_iatrain(self):
        try:
            categorie = 'Bague'
            field = get_field()

            bijoux = self.session.query(Bijou).filter_by(banned=False, categorie=categorie)

            res = {}
            for bijou in bijoux:
                if 'p_'+field in bijou.data:
                    print('ok')
                    if str(bijou.data['p_'+field]) not in res:
                        res[str(bijou.data['p_'+field])] = []

                    res[str(bijou.data['p_'+field])].append({'id': bijou.id, 'image': bijou.images['thumb']['url']})

            return res
        except:
            print_error('get ia_train')
            return {'error': 'error'}

    # sauvegarde liste de bijou
    def post_iatrain(self, data):
        try:
            field = get_field()

            for elm in data:
                bijou = self.session.query(Bijou).filter_by(id=elm['id']).first()
                if bijou is None:
                    return {'error': 'bijou not found'}

                bijou.data[field] = elm[field]
                del bijou.data['p_'+field]
                self.session.add(bijou)

            self.session.commit()
            return 'ok'
        except:
            self.session.rollback()
            print_error('iatrain error')
            return {'error': 'error'}


def print_error(user_id):

    traceback.print_exc()

    with open("error.txt", "a") as myfile:
        myfile.write('\n\n\n' + time.strftime("%c") + '\n'
                     + '-------------------------------------------------\n\n'
                     + 'User_id : '+user_id+'\n\n'
                     + '-------------------------------------------------\n\n'
                     + traceback.format_exc() + '\n')
