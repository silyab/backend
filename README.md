# Démarrage #
```
cd backend/
screen 
source ../smart_env/bin/activate
uwsgi config/uwsgi_config.ini
```
```
cd frontend/
screen ./node_modules/.bin/ng serve --host 51.254.34.211 --port 80 --live-reload-port 49153 -prod

```

# uWSGI control
list all :
```
ps ax | grep uwsgi
```
kill all :
```
killall -s INT ~/virtual_envs/smart/bin/uwsgi
```
kill :
```
kill -HUP `cat /tmp/smart_server.pid`
```
reload :
```
uwsgi --reload /tmp/smart_server.pid
```

# Update #


# Install #

# Python virtualenv
```
apt-get install python-virtualenv
virtualenv -p python3 smart_env
source smart_env/bin/activate
```

# Pip
```
pip install falcon
pip install falcon-cors
pip install sqlalchemy
pip install uwsgi
```

# Postgre
```
apt-get install postgresql postgresql-client
apt-get install python-psycopg2
apt-get install libpq-dev
apt-get install python3-dev
apt-get install gcc
```

# Créer utilisateur postgre et Dbs
```
sudo -u postgres bash
createuser -P smart
createdb -O smart smart_bijoux
createdb -O smart smart_users
```

si erreur : psql: FATAL:  Ident authentication failed for user "mypguser"

alors modifier /etc/postgresql/X.Y/main/pg_hba.conf :
```
local   all         all         trust     # replace ident or peer with trust
```

# Xml support
```
apt-get install libxml2-dev libxslt1-dev
pip install lxml
```

# Note #

Ajouter colonne
```
ALTER TABLE bijou ADD COLUMN montre_forme character varying(12);
```