from sqlalchemy import create_engine, Boolean, Text
from config import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, PickleType
from sqlalchemy.ext.mutable import MutableDict

engine = create_engine(DB)
engine_legacy = create_engine(DB_legacy)
Base = declarative_base(bind=engine)


class UserModel(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    user_id = Column(String(20), unique=True)

    data = Column(MutableDict.as_mutable(PickleType))
    style = Column(MutableDict.as_mutable(PickleType))
    monitoring = Column(MutableDict.as_mutable(PickleType))


class BijouModel(Base):
    __tablename__ = 'bijoux'

    id = Column(Integer, primary_key=True)
    product_id = Column(String(30), unique=True)

    treated = Column(Boolean)
    banned = Column(Boolean)

    source = Column(String(12))
    categorie = Column(String(12))
    nom = Column(Text)
    description = Column(Text)
    prix = Column(String(10))
    marque = Column(String(30))

    images = Column(MutableDict.as_mutable(PickleType))
    url = Column(Text)

    data = Column(MutableDict.as_mutable(PickleType))
    # ancien_prix
    # matiere
    # size
    # taille
    # occasion
    # qualite
    # couleur_metal
    # couleurs
    # collier_longeur collier_forme bague_monture boucle_forme
    # styles


def generate_all():
    Base.metadata.create_all(engine)

if __name__ == '__main__':
    generate_all()
