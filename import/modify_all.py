import traceback

from sqlalchemy.orm import Session

from Bijou import Bijou
from Db import engine

session = Session(bind=engine)
bijoux = session.query(Bijou).filter_by()

for bijou in bijoux:
    try:
        if 'p_taille' in bijou.data:
            del bijou.data['p_taille']

        session.add(bijou)
    except:
        session.rollback()
        traceback.print_exc()
        exit()

session.commit()
