import sys
import traceback

from lxml import etree
from sqlalchemy.orm import Session

from Db import engine
from Bijou import Bijou

session = Session(bind=engine)
tree = etree.parse("forzieri_19_05_17.xml")
root = tree.getroot()
products = root[0]

i = 0
for product in products:

    categorie = False
    gender = False
    data = {}

    data['product_id'] = product.attrib['id']
    data['treated'] = False
    data['banned'] = False
    data['source'] = 'forzieri'

    data['images'] = {}
    data['data'] = {}

    for child in product:

        if child.tag == 'cat':
            for child2 in child:
                if child2.tag == 'merchantProductCategoryPath':

                    if child2.text[-8:] == 'Colliers':
                        data['categorie'] = 'Collier'
                        categorie = True

                    if child2.text[-9:] == 'Bracelets':
                        data['categorie'] = 'Bracelet'
                        categorie = True

                    if child2.text[-7:] == 'oreille':
                        data['categorie'] = 'Boucle_oreil'
                        categorie = True

                    if child2.text[-6:] == 'Bagues':
                        data['categorie'] = 'Bague'
                        categorie = True

        if child.tag == 'text':
            for child2 in child:
                if child2.tag == 'name':
                    data['nom'] = child2.text
                if child2.tag == 'desc':
                    data['description'] = child2.text

        if child.tag == 'price':
            for child2 in child:
                if child2.tag == 'buynow':
                    data['prix'] = child2.text
                if child2.tag == 'productPriceOld':
                    data['data']['ancien_prix'] = child2.text.replace(',', '.')

        if child.tag == 'custom1':
            data['marque'] = child.text

        if child.tag == 'uri':
            for child2 in child:
                if child2.tag == 'awTrack':
                    data['url'] = child2.text

                if child2.tag == 'mThumb':
                    data['images']['thumb'] = {'url': child2.text, 'hauteur': 300, 'decalage': 0}

        if child.tag == 'vertical':
            for child2 in child:
                if child2.tag == 'material':
                    data['data']['matiere'] = child2.text

                if child2.tag == 'suitableFor' and child2.text == 'Femme':
                    gender = True

    # nettoyage nom, description et ancien_prix
    for key in ['nom', 'description']:
        if data[key][:len(data['marque'])] == data['marque']:
            data[key] = data[key][len(data['marque']):]
            while data[key][:1] in [' ', '-']:
                data[key] = data[key][1:]

    if float(data['prix']) >= float(data['data']['ancien_prix']):
        del data['data']['ancien_prix']

    if categorie and gender:
        bijou = session.query(Bijou).filter_by(product_id=data['product_id']).first()
        if bijou is None:
            try:
                new_bijou = Bijou(data)
                session.add(new_bijou)
            except:
                session.rollback()
                traceback.print_exc()
                exit()
        else:
            if bijou.prix != data['prix']:
                try:
                    bijou.prix = data['prix']
                    if 'ancien_prix' in data['data']:
                        bijou.data['ancien_prix'] = data['data']['ancien_prix']
                    if float(bijou.prix) == float(bijou.data['ancien_prix']):
                        del bijou.data['ancien_prix']
                    session.add(bijou)
                except:
                    session.rollback()
                    traceback.print_exc()
                    exit()

session.commit()
