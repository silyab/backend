import traceback

from sqlalchemy.orm import Session

from Bijou import Bijou
from Db import engine
import urllib.request
from lxml import html

session = Session(bind=engine)
bijoux = session.query(Bijou).filter_by(source='stylebop')

k = 1
for bijou in bijoux:
    if len(bijou.images) == 1:
        try:
            page = html.fromstring(urllib.request.urlopen(bijou.url).read())
            print(bijou.url)

            elems = page.xpath(
                "//div[contains(@class, 'product-image-gallery')]/div[contains(@class, 'small-image')]/img")
            i = len(elems)-1
            j = len(elems)
            for img in elems:
                key = 'mini_'+str(i)
                key2 = 'image_'+str(i)

                bijou.images[key] = {'url': 'https:'+img.get('src'), 'hauteur': 100, 'decalage': 0}
                if i != 0:
                    bijou.images[key2] = {'url': 'https:'+img.get('data-zoom-image'), 'hauteur': 300, 'decalage': 0}

                i -= 1

            elems = page.xpath(
                "//div[contains(@class, 'product-image-gallery')]/div[contains(@class, 'big-image')]/img")
            for img in elems:
                key = 'mini_'+str(j)
                key2 = 'image_'+str(j)

                bijou.images[key] = {'url': 'https:'+img.get('src'), 'hauteur': 100, 'decalage': 0}
                bijou.images[key2] = {'url': 'https:'+img.get('data-zoom-image'), 'hauteur': 300, 'decalage': 0}

                j += 1

            print(bijou.images)
            session.add(bijou)
            print(str(k) + '/' + str(bijoux.count()))
            k += 1
        except:
            session.rollback()
            traceback.print_exc()
            exit()

session.commit()
