import traceback

import _pickle as cPickle
from sqlalchemy import MetaData, Table
from sqlalchemy.orm import Session, mapper

from Bijou import Bijou
from Db import engine_legacy, engine


class BijouLegacy(object):
    pass

metadata = MetaData(engine_legacy)
moz_bookmarks = Table('bijou', metadata, autoload=True)
mapper(BijouLegacy, moz_bookmarks)

session = Session(bind=engine)
session_legacy = Session(bind=engine_legacy)

bijoux_legacy = session_legacy.query(BijouLegacy).filter_by(treated=True)

for bijou_l in bijoux_legacy:
    bijou = session.query(Bijou).filter_by(product_id=bijou_l.zupid).first()
    if bijou is not None and False:
        try:
            bijou.data['taille'] = bijou_l.taille
            bijou.data['occasion'] = bijou_l.occasion
            bijou.data['qualite'] = bijou_l.qualite
            bijou.data['couleur_metal'] = bijou_l.couleur_metal.lower() if bijou_l.couleur_metal != 'Dore_mod' else 'dore'
            bijou.data['couleurs'] = [bijou_l.couleur_pierre.lower()] if bijou_l.couleur_pierre != 'Non' else []
            if bijou_l.collier_longueur is not None:
                bijou.data['collier_longueur'] = bijou_l.collier_longueur.lower()
            if bijou_l.collier_forme is not None:
                bijou.data['collier_forme'] = bijou_l.collier_forme.lower()
            if bijou_l.bague_monture is not None:
                bijou.data['bague_monture'] = bijou_l.bague_monture.lower()
            if bijou_l.boucle_forme is not None:
                bijou.data['boucle_forme'] = bijou_l.boucle_forme.lower()
            session.add(bijou)
        except:
            session.rollback()
            traceback.print_exc()
            exit()
    if bijou is not None and False:
        try:
            bijou.data['styles'] = []
            if bijou_l.style:
                styles = cPickle.loads(bijou_l.style)
                for key in ['moderne', 'classique', 'antique', 'ancien', 'simple', 'rock', 'petite_fille']:
                    if key in styles:
                        if key == 'petite_fille':
                            bijou.data['styles'].append('enfantin')
                        else:
                            bijou.data['styles'].append(key)

            if bijou_l.symbolique:
                symboliques = cPickle.loads(bijou_l.symbolique)
                if 'floral' in symboliques:
                    bijou.data['styles'].append('floral')

            session.add(bijou)
        except:
            session.rollback()
            traceback.print_exc()
            exit()

session.commit()
