import sys
import traceback

from lxml import etree
from sqlalchemy.orm import Session

from Db import engine
from Bijou import Bijou

session = Session(bind=engine)
tree = etree.parse("stylebop_21_05_17.xml")
root = tree.getroot()
products = root[0]

i = 0
for product in products:

    categorie = False
    gender = False
    data = {}

    data['product_id'] = product.attrib['id']
    data['treated'] = False
    data['banned'] = False
    data['source'] = 'stylebop'

    data['images'] = {}
    data['data'] = {}

    for child in product:

        if child.tag == 'cat':
            for child2 in child:
                if child2.tag == 'mCat':

                    if child2.text[-9:] == 'Necklaces':
                        data['categorie'] = 'Collier'
                        categorie = True

                    if child2.text[-9:] == 'Bracelets':
                        data['categorie'] = 'Bracelet'
                        categorie = True

                    if child2.text[-8:] == 'Earrings':
                        data['categorie'] = 'Boucle_oreil'
                        categorie = True

                    if child2.text[-5:] == 'Rings':
                        data['categorie'] = 'Bague'
                        categorie = True

        if child.tag == 'brand':
            for child2 in child:
                if child2.tag == 'brandName':
                    data['marque'] = child2.text

        if child.tag == 'text':
            for child2 in child:
                if child2.tag == 'name':
                    data['nom'] = child2.text
                if child2.tag == 'desc':
                    data['description'] = child2.text

        if child.tag == 'price':
            for child2 in child:
                if child2.tag == 'buynow':
                    data['prix'] = child2.text

        if child.tag == 'colour':
            if child.text == 'argent':
                data['data']['couleur_metal'] = 'argente'
            if child.text == 'or':
                data['data']['couleur_metal'] = 'dore'

        if child.tag == 'custom2' and child.text == 'women':
            gender = True

        if child.tag == 'uri':
            for child2 in child:
                if child2.tag == 'awTrack':
                    data['url'] = child2.text

                if child2.tag == 'largeImage':
                    data['images']['thumb'] = {'url': child2.text, 'hauteur': 300, 'decalage': 0}

    if categorie and gender:
        bijou = session.query(Bijou).filter_by(product_id=data['product_id']).first()
        if bijou is None:
            try:
                new_bijou = Bijou(data)
                session.add(new_bijou)
            except:
                session.rollback()
                traceback.print_exc()
                exit()
        else:
            if bijou.prix != data['prix']:
                try:
                    bijou.prix = data['prix']
                    session.add(bijou)
                except:
                    session.rollback()
                    traceback.print_exc()
                    exit()

session.commit()
