import traceback

from sqlalchemy.orm import Session

from Bijou import Bijou
from Db import engine
import urllib.request
from lxml import html

session = Session(bind=engine)
bijoux = session.query(Bijou).filter_by(source='histoire_dor')

j = 1
for bijou in bijoux:
    if len(bijou.images) == 1:
        try:
            page = html.fromstring(urllib.request.urlopen(bijou.url).read())
            print(bijou.url)
            i = 0
            for img in page.xpath("//ul[contains(@id, 'slider-pager')]/li/a[@data-slide-index]/img"):
                key = 'mini_'+str(i)
                key2 = 'image_'+str(i)

                bijou.images[key] = {'url': img.get('src'), 'hauteur': 100, 'decalage': 0}
                if i != 0:
                    bijou.images[key2] = {'url': img.get('src').replace('/thumb', '/view'), 'hauteur': 300, 'decalage': 0}

                i += 1

            print(bijou.images)
            session.add(bijou)
            print(str(j)+'/'+str(bijoux.count()))
            j += 1
        except:
            session.rollback()
            traceback.print_exc()
            exit()

session.commit()
