from Db import UserModel
from Silya import conseilBijoux


class User(UserModel):

    def __init__(self, user_id):
        self.user_id = user_id
        self.data = {'wishlist': {'actif': [], 'conseil': [], 'supprime': []}, 'silya': {}, 'filtres': {}}
        self.style = {}
        self.monitoring = {}

    def get_info(self):
        data = {}
        for key in ['silya', 'filtres']:
            data[key] = self.data[key]

        return data

    def update_info(self, data):
        for key, elm in data.items():
            self.data[key] = elm

    def get_wishlist(self, size):
        if size == 'short':
            return self.data['whislist']['actif']
        else:
            return self.data['whislist']

    def update_wishlist(self, bijou, add):
        if add:
            deja_present = False
            for elm in self.data['wishlist']['actif']:
                if elm.id == bijou.id:
                    deja_present = True
                    break

            if not deja_present:
                self.data['wishlist']['actif'].append(bijou.simple_form())
                self._updateWishlistConseil()
        else:
            i = 0
            for elm in self.data['wishlist']['actif']:
                if elm.id == bijou.id:
                    supprime = self.data['wishlist']['actif'].pop(i)
                    self.data['wishlist']['supprime'].append(supprime)
                    break
            self._updateWishlistConseil()

    def email_wishlist(self, email):
        print('email sent to '+email)

    def _updateWishlistConseil(self):
        self.data['wishlist']['conseil'] = conseilBijoux(self.data['wishlist']['actif'])
