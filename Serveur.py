import falcon
import json

from Manager import Manager
from falcon_cors import CORS

from config import DEBUG

cors = CORS(allow_all_origins=True,
            allow_all_headers=True,
            allow_methods_list=['POST', 'OPTIONS', 'GET'])
manager = Manager()


class JSONTranslator(object):

    def process_request(self, req, resp):
        # req.stream corresponds to the WSGI wsgi.input environ variable,
        # and allows you to read bytes from the request body.
        #
        # See also: PEP 3333
        if req.content_length in (None, 0):
            # Nothing to do
            return

        body = req.stream.read()
        if not body:
            raise falcon.HTTPBadRequest('Empty request body',
                                        'A valid JSON document is required.')

        try:
            req.context['json'] = json.loads(body.decode('utf-8'))

        except (ValueError, UnicodeDecodeError):
            raise falcon.HTTPError(falcon.HTTP_753,
                                   'Malformed JSON',
                                   'Could not decode the request body. The '
                                   'JSON was incorrect or not encoded as '
                                   'UTF-8.')

    def process_response(self, req, resp, resource):
        if 'result' not in req.context:
            return

        resp.body = json.dumps(req.context['result'])


class UserIdResource:
    def on_get(self, req, resp):
        result = manager.get_new_id()
        resp.body = json.dumps(result)


class UserResource:
    def on_get(self, req, resp, user_id):
        result = manager.test_id(user_id)
        resp.body = json.dumps(result)

    def on_post(self, req, resp, user_id):
        result = manager.create_user(user_id)
        resp.body = json.dumps(result)


class UserInfoResource:
    def on_get(self, req, resp, user_id):
        result = manager.info_user(user_id)
        resp.body = json.dumps(result)

    def on_post(self, req, resp, user_id):
        result = manager.update_user(user_id, req.context['json'])
        resp.body = json.dumps(result)


class WishlistResource:
    def on_get(self, req, resp, user_id, size):
        result = manager.get_wishlist(user_id, size)
        resp.body = json.dumps(result)

    def on_post(self, req, resp, user_id, size):
        result = manager.update_wishlist(user_id, req.context['json'])
        resp.body = json.dumps(result)


class EmailWishlistResource:
    def on_post(self, req, resp, user_id):
        result = manager.email_wishlist(user_id, req.context['json'])
        resp.body = json.dumps(result)


class BijouResource:
    def on_get(self, req, resp, user_id, bijou_id):
        result = manager.get_info_bijou(user_id, bijou_id)
        resp.body = json.dumps(result)


class SelectionUserResource:
    def on_get(self, req, resp, user_id):
        result = manager.get_user_selection(user_id)
        resp.body = json.dumps(result)


class SelectionResource:
    def on_get(self, req, resp, tag, size):
        result = manager.get_selection(tag, size)
        resp.body = json.dumps(result)


class TrainerResource:
    def on_get(self, req, resp, bijou_id):
        if not DEBUG:
            return json.dumps({'error': 'error'})

        result = manager.get_train(bijou_id)
        resp.body = json.dumps(result)

    def on_post(self, req, resp, bijou_id):
        if not DEBUG:
            return json.dumps({'error': 'error'})

        result = manager.post_train(bijou_id, req.context['json'])
        resp.body = json.dumps(result)


class IaTrainerResource:
    def on_get(self, req, resp):
        if not DEBUG:
            return json.dumps({'error': 'error'})

        result = manager.get_iatrain()
        resp.body = json.dumps(result)

    def on_post(self, req, resp):
        if not DEBUG:
            return json.dumps({'error': 'error'})

        result = manager.post_iatrain(req.context['json'])
        resp.body = json.dumps(result)


api = falcon.API(middleware=[JSONTranslator(), cors.middleware])
api.add_route('/user_id', UserIdResource())
api.add_route('/user/{user_id}', UserResource())
api.add_route('/user/{user_id}/info', UserInfoResource())
api.add_route('/user/{user_id}/wishlist/{size}', WishlistResource())
api.add_route('/user/{user_id}/wishlist/email', EmailWishlistResource())
api.add_route('/user/{user_id}/bijou/{bijou_id}', BijouResource())
api.add_route('/user/{user_id}/selection', SelectionUserResource())
api.add_route('/selection/{tag}/{size}', SelectionResource())
api.add_route('/train/{bijou_id}', TrainerResource())
api.add_route('/iatrain', IaTrainerResource())
