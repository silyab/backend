from Db import BijouModel
from Meta import info_source
from Silya import conseilBijoux, conseilSilya


class Bijou(BijouModel):

    def __init__(self, data):
        for key, value in data.items():
            self.__setattr__(key, value)

    def simple_form(self):
        res = {
            'id': self.id,
            'nom': self.nom,
            'categorie': self.categorie,
            'marque': self.marque,
            'prix': self.prix,
            'img': self.images['thumb'],
            'url': self.url
        }

        if 'ancien_prix' in self.data:
            res['ancien_prix'] = self.data['ancien_prix']

        return res

    def get_info(self, user):
        conseil_silya = conseilSilya(self, user)
        res = {
            'id': self.id,
            'nom': self.nom,
            'description': self.description,
            'categorie': self.categorie,
            'marque': self.marque,
            'source': info_source(self.source),
            'prix': self.prix,
            'matiere': self.data['matiere'],
            'images': self.images,
            'url': self.url,
            'craque': conseil_silya['craque'],
            'silya': conseil_silya['morpho'],
            'conseil': conseilBijoux([self]),
        }

        if 'ancien_prix' in self.data:
            res['ancien_prix'] = self.data['ancien_prix']

        return res

    def train_form(self):
        keys = ['id', 'product_id', 'treated', 'banned', 'categorie', 'nom', 'description', 'prix',
                'marque', 'images', 'url', 'data']
        res = {}

        for key in keys:
            res[key] = getattr(self, key)

        print(res)
        return res

    def set_data(self, data):
        for key, value in data.items():
            setattr(self, key, value)
